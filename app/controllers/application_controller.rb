class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  @users = User.all
  @students = Student.all
  @briefs = Brief.all
  @courses = Course.all

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected 

  def configure_permitted_parameters
    added_attrs = [:matricula, :email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end
end

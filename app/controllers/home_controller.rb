class HomeController < ApplicationController
  def index
    @users = User.all
    @students = Student.all
    @briefs = Brief.all
    @courses = Course.all
  end

  def talentos
    @users = User.all
    @students = Student.all
    @briefs = Brief.all
    @courses = Course.all
  end

  def depoimentos
    @users = User.all
    @students = Student.all
    @briefs = Brief.all
    @courses = Course.all
  end

end

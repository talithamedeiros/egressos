json.extract! brief, :id, :texto, :data, :student_id, :created_at, :updated_at
json.url brief_url(brief, format: :json)

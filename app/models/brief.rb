class Brief < ApplicationRecord
  belongs_to :student

  enum homologado: [:pendente, :homologado, :nao_homologado]
  
  validates :texto, presence: true
end

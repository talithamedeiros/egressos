class Coordinator < ApplicationRecord
	belongs_to :course

	validates :nome, presence: true, length: {in: 3 .. 60}
	validates :matricula, presence: true, uniqueness: {case_sensitive: true}, length:{is:11}
	validates :senha, presence: true, length: {is: 6}
	validates :email, presence: true, uniqueness: { case_sensitive: true }
end

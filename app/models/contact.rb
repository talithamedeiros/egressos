class Contact < ApplicationRecord
  belongs_to :student

  validates :tipo, presence: true, inclusion: {in: %w(email telefone facebook instagram linkedin twitter github) }
  validates :valor, presence: true, length: {in: 2 .. 60}
end

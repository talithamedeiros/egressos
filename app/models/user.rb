class User < ApplicationRecord
  enum role: [:normal_user, :student, :coordinator, :admin]
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, authentication_keys: [:matricula]

  validates :matricula, presence: :true, uniqueness: { case_sensitive: true }
  validates :email, presence: :true, uniqueness: { case_sensitive: true }
end

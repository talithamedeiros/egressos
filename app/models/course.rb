class Course < ApplicationRecord
	has_one :coordinator
	has_many :students

	validates :nome, presence: true, length: {in: 3 .. 60}
	validates :sigla, presence: true, length: {in: 2 .. 10}
	validates :site, presence: true, length: {in: 6 .. 100}

	accepts_nested_attributes_for :coordinator
end

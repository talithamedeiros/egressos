class Student < ApplicationRecord
  belongs_to :course

  has_many :contacts
  has_one :brief


  validates :nome, presence: true, length: {in: 3 .. 60}
  validates :matricula, presence: true, uniqueness: { case_sensitive: true }, length: {is: 11}
  validates :senha, presence: true, length: {is: 6}
  validates :ano, presence: true, numericality: {only_integer: true}
  validates :semestre, presence: true, numericality: {only_integer: true}
  validates :email, presence: true, uniqueness: { case_sensitive: true }

  enum perfil: [:privado, :publico]
  enum homologado: [:pendente, :nao_homologado, :homologado]

  accepts_nested_attributes_for :contacts, reject_if: :all_blank, allow_destroy: :true
end

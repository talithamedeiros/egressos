class AddEmailToCoordinator < ActiveRecord::Migration[5.1]
  def change
    add_column :coordinators, :email, :string
  end
end

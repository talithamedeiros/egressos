class AddHomologadoToStudent < ActiveRecord::Migration[5.1]
  def change
    add_column :students, :homologado, :integer, :default=>0
  end
end

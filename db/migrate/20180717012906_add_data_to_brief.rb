class AddDataToBrief < ActiveRecord::Migration[5.1]
  def change
    add_column :briefs, :data, :date
  end
end

class RemoveDataFromBrief < ActiveRecord::Migration[5.1]
  def change
    remove_column :briefs, :data, :datetime
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# role: [:normal_user, :student, :coordinator, :admin]

User.create!(
    email: "admin@admin.com",
    password: "123456",
    matricula: "10101010101",
    role: 3
)

Course.create!(
    nome: "Sistemas para Internet",
    sigla: "SI",
    site: "https://estudante.ifpb.edu.br/cursos/39",
    coordinator_attributes: {
        nome: "Cândido Egypto",
        matricula: "20202020202",
        email: "candido@coord.com",
        senha: "123456"
    }
)
User.create!(
    email: "candido@coord.com",
    password: "123456",
    matricula: "20202020202",
    role: 2
)

Course.create!(
    nome: "Redes de Computadores",
    sigla: "RC",
    site: "https://estudante.ifpb.edu.br/cursos/37",
    coordinator_attributes: {
        nome: "Leandro Almeida",
        matricula: "30303030303",
        email: "leandro@coord.com",
        senha: "123456"
    }
)
User.create!(
    email: "leandro@coord.com",
    password: "123456",
    matricula: "30303030303",
    role: 2
)

Student.create!(
    nome: "João da Silva",
    matricula: "20122373325",
    email: "joao@gmail.com",
    senha: "123456",
    ano: 2016,
    semestre: 2,
    course: Course.all.sample
    ).contacts.create!(
        [
            {tipo: "email", valor: "joao@gmail.com"},
            {tipo: "linkedin", valor: "linkedin.com/joadasilvaaaaa"},
            {tipo: "github", valor: "github.com/joaodasilvaaaaaa"}
        ]
    )
User.create!(
    email: "joao@gmail.com",
    password: "123456",
    matricula: "20122373325",
    role: 0
)


Student.create!(
    nome: "Maria de Medeiros",
    matricula: "20092393112",
    email: "maria@gmail.com",
    senha: "123456",
    ano: 2012,
    semestre: 1,
    course: Course.all.sample
    ).contacts.create!(
        [
            {tipo: "email", valor: "maria@gmail.com"},
            {tipo: "linkedin", valor: "linkedin.com/maria"},
            {tipo: "github", valor: "github.com/maria"}
        ]
    )
User.create!(
    email: "maria@gmail.com",
    password: "123456",
    matricula: "20092393112",
    role: 0
)


Student.create!(
    nome: "Gustavo de Souza",
    matricula: "20142393238",
    email: "gustavo@gmail.com",
    senha: "123456",
    ano: 2017,
    semestre: 2,
    course: Course.all.sample
    ).contacts.create!(
        [
            {tipo: "email", valor: "gustavo@gmail.com"},
            {tipo: "linkedin", valor: "linkedin.com/gustavo"},
            {tipo: "github", valor: "github.com/gustavo"}
        ]
    )
User.create!(
    email: "gustavo@gmail.com",
    password: "123456",
    matricula: "20142393238",
    role: 0
)


Brief.create!(
    texto: "Ótima experiência com este curso incrível.",
    student: Student.all.sample,
    data: Date.today
)

Brief.create!(
    texto: "Aprendizado para o resto da vida.",
    student: Student.all.sample,
    data: Date.today
)
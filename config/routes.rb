Rails.application.routes.draw do
  get 'home/index'
  get 'home/talentos'
  get 'home/depoimentos'

  root 'home#index'

  resources :students
  resources :courses
  resources :briefs
  
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
